package com.developers.discordbot;

import java.io.File;
import java.nio.file.Files;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

public class MAIN {
    public static void main(String[] args) throws Exception {
        DiscordApi api = new DiscordApiBuilder().setToken(Files.readAllLines(new File("token.txt").toPath()).get(0)).login().join();
    }
}
